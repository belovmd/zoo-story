class ZooInspector(object):
    def __init__(self, image_recognition_system, inspection_log):
        self.image_recognition_system = image_recognition_system
        self.inspection_log = inspection_log

    def inspect(self, zoo):
        inspection = Inspection(zoo, self.image_recognition_system)
        inspection.run_inspection()
        self.inspection_log.log(inspection.inspection_statuses)


class Inspection(object):
    def __init__(self, zoo, image_recognition_system):
        self.zoo = zoo
        self.image_recognition_system = image_recognition_system
        self.inspection_statuses = []
        self.zoo_warning_status = False

    def run_inspection(self):
        self.inspect_enclosures_and_animals()
        self.report_zoo_status()

    def inspect_enclosures_and_animals(self):
        for enclosure in self.zoo.get_enclosures():
            self.inspect_enclosure(enclosure)
            self.inspect_animal(enclosure)

    def report_zoo_status(self):
        zoo_status = "WARNING" if self.zoo_warning_status else "OK"
        self.report_status('ZOO', self.zoo.get_id(), zoo_status)

    def report_enclosure_warning_status(self, enclosure):
        self.report_status('ENCLOSURE', enclosure.get_id(), 'WARNING')

    def report_animal_warning_status(self, animal):
        self.report_status('ANIMAL', animal.get_name(), 'WARNING')
    
    def report_status(self, object_name, object_id, status):
        self.inspection_statuses.append(f'{object_name}#{object_id}#{status}')

    def inspect_enclosure(self, enclosure):
        enclosure_image = self.zoo.capture_picture_of(enclosure)
        enclosure_status = self.image_recognition_system.recognize_enclosure_status(enclosure, enclosure_image)
        if (not enclosure_status.is_enclosure_safe()):
            self.respond_to_not_safe_enclosure(enclosure)
            self.report_not_safe_enclosure_status(enclosure)
    
    def respond_to_not_safe_enclosure(self, enclosure):
        self.zoo.close_enclosure(enclosure)
        self.zoo.request_security_to(enclosure)
        self.zoo.request_maintenance_crew_to(enclosure)

    def report_not_safe_enclosure_status(self, enclosure):
        self.report_enclosure_warning_status(enclosure)
        self.zoo_warning_status = True

    def inspect_animal(self, enclosure):
        animal_image = self.zoo.capture_picture_of(enclosure.get_animal())
        animal_status = self.image_recognition_system.recognize_animal_status(enclosure.get_animal(), animal_image)
        if (animal_status.is_animal_sick()):
            self.respond_to_sick_animal(enclosure)
            self.report_sick_animal_status(enclosure.get_animal())
    
    def respond_to_sick_animal(self, enclosure):
        self.zoo.close_enclosure(enclosure)
        self.zoo.request_veterinary_to(enclosure.get_animal())

    def report_sick_animal_status(self, animal):
        self.report_animal_warning_status(animal)
        self.zoo_warning_status = True
