---
focus: src/zoo_inspector.py:32-39
---
### Status-specific methods

Let's introduce [additional methods](src/zoo_inspector.py:32-39).

First, a [method](src/zoo_inspector.py:38-39) to add generic status to
`inspection_statuses` providing object name, id, and status.

Second, a special method to report a [warning of sick animal](src/zoo_inspector.py:35-36). I haven't
named it `report_animal_status` as it can be used to report status of only sick animal but not healthy one.

Third, a special method to report warning of [not safe enclosure](src/zoo_inspector.py:32-33).
I haven't named it `report_enclosure_status` for the same reasons as for an animal status method.

Lets use [new methods](src/zoo_inspector.py:32-39) now.
I have modified [report_zoo_status](src/zoo_inspector.py:28-30)
to use a new generic one.
I have replaced `add_warning_to_statuses` with new methods in
[report_not_safe_enclosure_status](src/zoo_inspector.py:54)
[report_sick_animal_status](src/zoo_inspector.py:68-69).
